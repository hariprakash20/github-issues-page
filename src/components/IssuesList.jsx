import React, { Component } from "react";
import EachIssue from "./EachIssue";

export class IssuesList extends Component {
  render() {
    const { issues, status, isLoading, isError, page, changeNextPage, changePrevPage } = this.props;

    return isError ? (
      <h1>Error occured in Fetching the data</h1>
    ) : isLoading ? (
      <div className="loader"></div>
    ) : (
      <div>
        <div className="heading">
          <h1>Issues for rails/rails</h1>
          <h2>page:{page}</h2>
        </div>
        {issues.length ? (
          <>
            {issues.map((issue) => {
              return <EachIssue key={issue.id} issue={issue} page={page}/>;
            })}
            <div className="buttons">
              <button onClick={changePrevPage} value="prev">prev</button>
              <button onClick={changeNextPage} value="next">next</button>
            </div>
          </>
        ) : (
          <div>
            <h1>No issue found</h1>
          </div>
        )}
      </div>
    );
  }
}

export default IssuesList;
