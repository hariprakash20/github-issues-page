import "./App.css";
import IssuesList from "./components/IssuesList";
import { Route, Routes } from "react-router-dom";
import React, { Component } from "react";
import IssueDetailsPage from "./components/IssueDetailsPage";

export class App extends Component {
  constructor(props) {
    super(props)

    this.API_STATES = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
  }

    this.state = { issues: [], status:this.API_STATES.LOADING , isLoading: true, isError: false, page: 1 };
    
  }
  

  componentDidMount() {
    fetch(
      `https://api.github.com/repos/rails/rails/issues?page=${this.state.page}`
    )
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        this.setState({ ...this.state, issues: response, isLoading: false ,status: this.API_STATES.LOADED});
      })
      .catch((err) => {
        this.setState({ ...this.state, isLoading: false, isError: true , status: this.API_STATES.ERROR});
      });
  }

  componentDidUpdate(prevProps, prevStates) {
    if (prevStates.page !== this.state.page) {
      fetch(`https://api.github.com/repos/rails/rails/issues?page=${this.state.page}`)
      .then((response) => {
        return response.json();
      })
      .then((response) => {
        this.setState({ ...this.state, issues: response, isLoading: false });
      })
      .catch((err) => {
        this.setState({ ...this.state, isLoading: false, isError: true });
      })
    }
    window.scrollTo({ top: 0, left: 0, behavior: "smooth" });
  }

  changeNextPage(page) {
    this.setState({ ...this.state, page: page + 1 });
  }

  changePrevPage(page) {
    if (page > 1) this.setState({ ...this.state, page: page - 1 });
  }

  render() {
    const { issues, status, isLoading, isError, page } = this.state;
    return (
      <div className="App">
        <Routes>
          <Route
            path="/"
            element={
              <IssuesList
                issues={issues}
                status={status}
                isLoading={isLoading}
                isError={isError}
                page={page}
                changeNextPage={this.changeNextPage.bind(this, page)}
                changePrevPage={this.changePrevPage.bind(this, page)}
              />
            }
          />
          <Route
            exact
            path="/:issue"
            element={<IssueDetailsPage page={page} />}
          />
        </Routes>
      </div>
    );
  }
}

export default App;
