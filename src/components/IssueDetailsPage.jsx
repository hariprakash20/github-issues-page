import React, { Component } from "react";
import { Link } from "react-router-dom";

export class IssueDetailsPage extends Component {
  state = { issue: [], isLoading: true, isError: false };
  async componentDidMount() {
    try {
      const response = await fetch(
        `https://api.github.com/repos/rails/rails/issues?page=${this.props.page}`
      );
      const data = await response.json();
      const issueNumber = window.location.pathname.slice(1);
      const issue = data.filter((eachIssue) => eachIssue.number == issueNumber);
      this.setState({ ...this.state, issue: issue[0], isLoading: false});
    } catch (err) {
      this.setState({ ...this.state, isLoading: false, isError: true });
    }
  }

  render() {
    const { issue, isLoading, isError } = this.state;
    return isError ? (
      <h1>Error occured in Fetching the data</h1>
    ) : isLoading ? (
      <div className="loader"></div>
    ) : (
      <>
        <Link to="/">
          <button>back</button>
        </Link>
        {issue ? (
          <>
            <div className="title">
              <h1>{issue.title}</h1>
            </div>
            <div className="issue-number-details">{issue.number}</div>
            <div className="state">{issue.state}</div>
            <div className="user-info">
              <img
                src={issue.user.avatar_url}
                alt="avatar"
                className="user-avatar"
              />
              <div>{issue.user.login}</div>
            </div>
            <hr />
            <div className="issue-detail-body">{issue.body}</div>
            <hr />

          </>
        ) : (
          <div>
            <h1>No issue found</h1>
          </div>
        )}
      </>
    );
  }
}

export default IssueDetailsPage;
