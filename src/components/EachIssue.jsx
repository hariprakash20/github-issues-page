import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./style.css";

export class EachIssue extends Component {
  render() {
    return (
      <>
      <Link to={`/${this.props.issue.number}`}>
      <div className="issue-tile">
          <div className="user-info">
            <img
              src={this.props.issue.user.avatar_url}
              alt="avatar"
              className="user-avatar"
            />
            <div>{this.props.issue.user.login}</div>
          </div>
          <div className="user-issue">
            <div className="issue-title">
              <span className="issue-number">#{this.props.issue.number}  </span>
              <b>{this.props.issue.title}</b>
            </div>
            <div className="issue-body">
              {this.props.issue.body} ...
            </div>
          </div>
        </div>
      </Link>

      </>
    );
  }
}

export default EachIssue;
